<?php

namespace App\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class CustomRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|min:2|unique:customs,name',
                    'title' => 'required|min:2',
                    'text' => 'nullable|string|min:2',
                    'photo'=>'nullable|image|mimes:jpeg,bmp,png|max:4096'
                ];
            }
            case 'PATCH':
            case 'PUT':
            {
                return [
                    'name' => 'required|string|min:2|unique:customs,name,NULL,id,deleted_at,NULL'.$this->id,
                    'title' => 'required|string|min:2',
                    'text' => 'nullable|string|min:2',
                    'photo'=>'sometimes|image|mimes:jpeg,bmp,png|max:4096'
                ];
            }
            default:break;
        }
    }

    public function messages()
    {
        return [
            'title.required' => __('admin/app.title_required'),
            'text.required' => __('admin/app.text_required'),
            'photo.required' => __('admin/app.photo_required'),
            'photo.image' => __('admin/app.photo_should_be_image')
        ];
    }

}
