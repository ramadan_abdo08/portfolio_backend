<?php

namespace Database\Seeders;

use App\Models\Info;
use App\Models\SliderOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infos')->delete();
        DB::table('slider_options')->delete();
        Info::create(['option' => 'website_name','value' => 'Softgates','type' => 'string']);
        Info::create(['option' => 'logo','value' => 'logo.png','type' => 'image']);
        Info::create(['option' => 'about_image','value' => 'about.png','type' => 'image']);
        Info::create(['option' => 'footer_image','value' => 'footer.png','type' => 'image']);
        Info::create(['option' => 'contact_image','value' => 'contact.png','type' => 'image']);
        Info::create(['option' => 'email','value' => '','type' => 'string']);
        Info::create(['option' => 'phone','value' => '','type' => 'string']);
        Info::create(['option' => 'address','value' => '','type' => 'string']);
        Info::create(['option' => 'fb_link','value' => '','type' => 'string']);
        Info::create(['option' => 'twitter_link','value' => '','type' => 'string']);
        Info::create(['option' => 'linked_link','value' => '','type' => 'string']);
        Info::create(['option' => 'instagram_link','value' => '','type' => 'string']);
        Info::create(['option' => 'about_us','value' => '','type' => 'text']);
        SliderOption::create(['image' => 'slidingoverlayhorizontal','word' => 'x:right']);

    }
}
