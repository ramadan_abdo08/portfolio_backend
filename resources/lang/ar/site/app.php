<?php

return [

    'home' => 'الصفحة الرئيسية',
    'Home' => 'الصفحة الرئيسية',
    'open_marketing' => 'افتح التسويق',
    'sign_in' => 'تسجيل الدخول',
    'sign_up' => 'اشتراك',
    'mazad' => 'مازاد',
    'buying_and_selling_information' => 'شراء وبيع المعلومات',
    'rest_information' => 'معلومات الراحة',
    'for_more_research' => 'لمزيد من البحث',
    'send_us_a_message' => 'أرسل لنا رسالة',
    'your_name' => 'اسمك',
    'your_email' => 'بريدك الالكتروني',
    'your_number' => 'رقمك',
    'your_message' => 'رسالتك',
    'send_email' => 'ارسل بريد الكتروني',
    'contact_us' => 'اتصل بنا',
    'view_project' => 'عرض مشروع',
    'projects' => 'المشاريع',
    'Projects' => 'المشاريع',
    'project_details' => 'تفاصيل المشروع',
    'view_service' => 'مشاهدة الخدمة',
    'services' => 'خدمات',
    'Services' => 'خدمات',
    'service_details' => 'تفاصيل الخدمة',
    'view_news' => 'مشاهدة الاخبار',
    'news' => 'أخبار',
    'News' => 'أخبار',
    'news_details' => 'تفاصيل الخبر',

    'About Us' => 'من نحن',
    'Contact Us' => 'تواصل معانا',

];
